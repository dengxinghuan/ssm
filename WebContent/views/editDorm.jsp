<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>新增学生入住信息</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/view.css"/>
</head>

<body>
<div class="layui-content">
    <div class="layui-row">
        <div class="layui-card">
            <div class="layui-card-header">宿舍信息</div>

            <form method="get" class="layui-form layui-card-body" action="${pageContext.request.contextPath}/dorm/editDorm">
            <input type="hidden" value="${dr.id}" name="id">
            
                <div class="layui-form-item">
                    <label class="layui-form-label">宿舍楼</label>
                    <div class="layui-input-block">
                        <input type="text" value="${dr.build_id}" name="build_id" required  lay-verify="required" placeholder="请输入学号" autocomplete="off" class="layui-input" style="width:190px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">宿舍号</label>
                    <div class="layui-input-block">
                        <input type="text" value="${dr.dorm_no}" name="dorm_no" required  lay-verify="required" placeholder="请输入姓名" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">已住人数</label>
                    <div class="layui-input-block">
                        <input type="text" value="${dr.count}" name="count" required  lay-verify="required" placeholder="请修改已住人数" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">最大人数</label>
                    <div class="layui-input-block">
                        <input type="text" value="${dr.max_num}" name="max_num" required  lay-verify="required" placeholder="请输入最大人数" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">状态</label>
                    <div class="layui-input-block">
                        <input type="radio" name="dorm_status" value="未满" title="未满" checked>
                        <input type="radio" name="dorm_status" value="已满" title="已满" >
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn layui-btn-blue" lay-submit lay-filter="formDemo">立即提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/assets/layui.all.js"></script>
<script>
    var form = layui.form
            ,layer = layui.layer;
</script>
</body>
</html>