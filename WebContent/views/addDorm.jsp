<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>新增宿舍楼</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/view.css"/>
</head>
<body>
<div class="layui-content">
    <div class="layui-row">
        <div class="layui-card">
            <div class="layui-card-header">添加宿舍</div>
            
            <form class="layui-form layui-card-body" action="${pageContext.request.contextPath}/dorm/addDorm" method="post">

                
                
                <div class="layui-form-item">
                    <label class="layui-form-label">新增宿舍楼</label>
                    <div class="layui-input-block">
                        <input type="text" name="build_id" required  lay-verify="required" placeholder="请输入宿舍楼id" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">新增宿舍号</label>
                    <div class="layui-input-block">
                        <input type="text" name="dorm_no" required  lay-verify="required" placeholder="请输入宿舍号" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
                
                <div class="layui-form-item">
                    <label class="layui-form-label">床位数</label>
                    <div class="layui-input-block">
                        <input type="text" name="max_num" required  lay-verify="required" placeholder="请输入宿舍最大人数" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>


                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn layui-btn-blue" lay-submit lay-filter="formDemo">提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            </form>
            
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/assets/layui.all.js"></script>
<script>
    var form = layui.form
            ,layer = layui.layer;
</script>
<script src="${pageContext.request.contextPath}/assets/jquery-3.4.1.js"></script>
</body>
</html>