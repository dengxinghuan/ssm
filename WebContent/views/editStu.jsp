<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>新增学生入住信息</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/css/layui.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/css/view.css" />
</head>

<!-- 
<script type="text/javascript">
		//设置类别的默认值
		//js中，不需要写变量的类型，t为参数变量，调用函数时可以把任何类型的值传给t
		function setUserBuildId(t) {
			//根据标签id获得标签对象
			var category = document.getElementById("build_id");
	       //获得下拉列表标签的所有选项，保存在数组ops中
			var ops = category.options;
			for ( var i = 0; i < ops.length; i++) {
				if (ops[i].value == t) {
					//如果选项值等于函数参数值，则把这个选项设置为默认选中项
					ops[i].selected = true;
					return;
				}
			}
		};
</script>
onload="setUserBuildId('${stu.build_id}')"
 -->



<body>
	<div class="layui-content">
		<div class="layui-row">
			<div class="layui-card">
				<div class="layui-card-header">学生信息</div>

				<form method="post" class="layui-form layui-card-body"
					action="${pageContext.request.contextPath}/student/editStudent">
					<input type="hidden" value="${stu.id}" name="id">

					<div class="layui-form-item">
						<label class="layui-form-label">学号</label>
						<div class="layui-input-block">
							<input type="text" value="${stu.no}" name="no" required
								lay-verify="required" placeholder="请输入学号" autocomplete="off"
								class="layui-input" style="width: 190px">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">姓名</label>
						<div class="layui-input-block">
							<input type="text" value="${stu.name}" name="name" required
								lay-verify="required" placeholder="请输入姓名" autocomplete="off"
								class="layui-input " style="width: 190px">
						</div>
					</div>

					<div class="layui-form-item">
						<label class="layui-form-label">性别</label>
						<div class="layui-input-block">
							<input type="radio" name="sex" value="男" title="男" checked>
							<input type="radio" name="sex" value="女" title="女">
						</div>
					</div>

					<div class="layui-form-item">
						<label class="layui-form-label">电话</label>
						<div class="layui-input-block">
							<input type="text" value="${stu.phone}" name="phone" required
								lay-verify="required" placeholder="请输入电话" autocomplete="off"
								class="layui-input " style="width: 190px">
						</div>
					</div>

					<%--  <div class="layui-form-item">
                    <div class="layui-form-label">宿舍楼</div>
                    <div class="layui-input-inline" style="width: 100px;">
                        <select name="build_id">
                            <c:forEach var = "dorm_id" items = "${dorm_id}"> 
                              <option value="${dorm_id.build_id}">${dorm_id.build_id}号楼</option>
                             </c:forEach>
                        </select>
                    </div>
                </div> --%>

					<!--   <div class="layui-form-item">
                    <div class="layui-form-label">房间</div>
                    <div class="layui-input-inline" style="width: 100px;">
                        <select name="dorm_no">
                            <option value="101">101</option>
                            <option value="102">102</option>
                            <option value="103">103</option>
                            
                        </select>
                    </div>
                </div>
 -->
					<div class="layui-form-item">
						<label class="layui-form-label">宿舍楼：</label>
						<div class="layui-input-block">
							<input type="text" value="${stu.build_id}" name="build_id" required
								lay-verify="required" autocomplete="off" class="layui-input "
								style="width: 190px">
						</div>
					</div>

					<div class="layui-form-item">
						<label class="layui-form-label">宿舍号：</label>
						<div class="layui-input-block">
							<input type="text" value="${stu.dorm_no}" name="dorm_no" required
								lay-verify="required" autocomplete="off" class="layui-input "
								style="width: 190px">
						</div>
					</div>


					<div class="layui-form-item">
						<div class="layui-input-block">
							<button class="layui-btn layui-btn-blue" lay-submit
								lay-filter="formDemo">立即提交</button>
							<button type="reset" class="layui-btn layui-btn-primary">重置</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
	<script src="${pageContext.request.contextPath}/assets/layui.all.js"></script>
	<script>
		var form = layui.form, layer = layui.layer;
	</script>
</body>
</html>