<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加缺勤记录</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/view.css"/>
</head>
<body>
<div class="layui-content">
    <div class="layui-row">
        <div class="layui-card">
            <div class="layui-card-header">添加缺勤记录</div>
            
            <form class="layui-form layui-card-body" action="${pageContext.request.contextPath}/record/addRecord" method="post">
                
                <div class="layui-form-item">
                    <label class="layui-form-label">日期：</label>
                    <div class="layui-input-block">
                        <input class="layui-input" id="test2" type="text" placeholder="请选择时间" name="date" required  lay-verify="required" autocomplete="off" style="width:190px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">学号：</label>
                    <div class="layui-input-block">
                        <input type="text" name="no" required  lay-verify="required" placeholder="请输入学号" autocomplete="off" class="layui-input" style="width:190px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">姓名：</label>
                    <div class="layui-input-block">
                        <input type="text" name="name" required  lay-verify="required" placeholder="请输入姓名" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
   
   				 <div class="layui-form-item">
                    <label class="layui-form-label">宿舍楼：</label>
                    <div class="layui-input-block">
                        <input type="text" name="build_id" required  lay-verify="required" placeholder="请输入宿舍楼(如1栋：1)" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
                
                 <div class="layui-form-item">
                    <label class="layui-form-label">宿舍号：</label>
                    <div class="layui-input-block">
                        <input type="text" name="dorm_no" required  lay-verify="required" placeholder="请输入宿舍号" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
  <!--  <div class="layui-form-item">
                    <div class="layui-form-label">宿舍楼：</div>
                    <div class="layui-input-inline" style="width: 100px;">
                        <select name="build_id">
                  			<option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                           
                        </select>
                    </div>
                </div>
   
                <div class="layui-form-item">
                    <div class="layui-form-label">宿舍号：</div>
                    <div class="layui-input-inline" style="width: 100px;">
                        <select name="dorm_no">
                  			<option value="101">101</option>
                            <option value="102">102</option>
                            <option value="103">103</option>
                            
                        </select>
                    </div>
                </div> -->
                
                 <div class="layui-form-item">
                    <div class="layui-form-label">备注：</div>
                    <div class="layui-input-inline" style="width: 100px;">
                        <select name="detail">
                  			<option value="请假">请假</option>
                            <option value="缺勤">缺勤</option>
                            <option value="晚归">晚归</option>
                           
                        </select>
                    </div>
                </div>
                
               <!--  <div class="layui-form-item">
                    <label class="layui-form-label">备注：</label>
                    <div class="layui-input-block">
                        <input type="text" name="detail" required  lay-verify="required" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div> -->

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn layui-btn-blue" lay-submit lay-filter="formDemo">提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            </form>
            
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/assets/layui.all.js"></script>
<script>
    var form = layui.form
            ,layer = layui.layer;
</script>
<script src="${pageContext.request.contextPath}/assets/jquery-3.4.1.js"></script>
<script type="text/javascript">
function showDept(obj)  
{  
    var dept = obj;           
    $.ajax({  
        type: "post",    
        url: "${pageContext.request.contextPath}/ListDormNoServlet",    
        data: "build_id="+dept,  
        async: false,  
        success: function(data){ 
           console.log(data) 
        }     
    });
}
</script>
<script type="text/javascript">
layui.use('laydate', function(){
  var laydate = layui.laydate;
  laydate.render({
    elem: '#test2'
    ,type: 'datetime'
  });
  });
  </script>
</body>
</html>