<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/view.css"/>
    <title>学生信息管理</title>
</head>
<body class="layui-view-body">
<form method="post" action="${pageContext.request.contextPath}/student/findStudentMany" id="listform">
<div class="layui-content">
    <div class="layui-page-header">
        <div class="pagewrap">
            <h2 class="title">学生入住管理</h2>
        </div>
    </div>
    <div class="layui-row">
        <div class="layui-card">
            <div class="layui-card-body">
                <div class="form-box">
                    <div class="layui-form layui-form-item">
                        <div class="layui-inline">
                        <!-- 查询name, sex, budld_id, dorm_no -->
                            <div class="layui-form-mid">学号:</div>
                            <div class="layui-input-inline" style="width: 100px;">
                                <input type="text" autocomplete="off" class="layui-input" name="no">
                            </div>
                            <div class="layui-form-mid">姓名:</div>
                            <div class="layui-input-inline" style="width: 100px;">
                                <input type="text" autocomplete="off" class="layui-input" name="name">
                            </div>
                            <div class="layui-form-mid">性别:</div>
                            <div class="layui-input-inline" style="width: 100px;">
                                <select name="sex">
                                	<option value="">请选择</option>
                                    <option value="男">男</option>
                                    <option value="女">女</option>
                                </select>
                            </div>
                              
                            <%-- <div class="layui-form-mid">宿舍楼:</div>
                            <div class="layui-input-inline" style="width: 100px;">
                                <select name="build_id">
                                	<option value="">请选择</option>
                               	<c:forEach var = "dorm_id" items = "${list}"> 
                                    <option value="${dorm_id.build_id}">${dorm_id.build_id}栋</option>
                                   </c:forEach>
                                </select>
                            </div> --%>
                            
                             <div class="layui-form-mid">宿舍楼:</div>
                            <div class="layui-input-inline" style="width: 100px;">
                                <input type="text" autocomplete="off" class="layui-input" name="build_id">
                            </div>
                            
                            <div class="layui-form-mid">宿舍号:</div>
                            <div class="layui-input-inline" style="width: 100px;">
                                <input type="text" autocomplete="off" class="layui-input" name="dorm_no">
                            </div>
                            <button class="layui-btn layui-btn-blue">查询</button>
                          
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
	                                              
	                        <a href="${pageContext.request.contextPath}/dorm/findBuildAll" class="layui-btn "><i class="layui-icon">&#xe654;</i>添加</a>
	                        <a id="output_table" class="layui-btn "><i class="layui-icon">&#xe654;</i>导出数据</a>
                        </div>
                    </div>
                  
                    <!--表内容放这里-->
                    <table id="table_content" class="layui-table" align="center">
                        <tr>
                        	<th>学号</th>
                            <th>姓名</th>
                            <th>性别</th>
                            <th>电话</th>
                            <th>宿舍楼</th>
                            <th>宿舍号</th>
                            <th>操作</th>
                        </tr>
                        <!--表内容-->
                         <c:forEach var = "stu" items = "${list}">
                        <tr>
                            <td style="text-align:center; padding-left:20px;">${stu.no}</td>
                            <td align="center">${stu.name}</td>
                            <td align="center">${stu.sex}</td>
                            <td align="center">${stu.phone}</td>
                            <td align="center">${stu.build_id}栋</td>
                            <td align="center">${stu.dorm_no}</td>
                            <td align="center">
                                <div class="button-group">
                                	<a href="${pageContext.request.contextPath}/student/findStudentId?id=${stu.id}" class="layui-btn layui-btn-blue" >修改</a>
                                    <a href="${pageContext.request.contextPath}/student/delStudent?id=${stu.id}&dorm_no=${stu.dorm_no}" class="layui-btn layui-btn-red" onclick="return Del(1)">删除</a>
                                                                        
                                </div>
                            </td>
                        </tr>
                         </c:forEach>
                    </table>
                    
                 
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script src="${pageContext.request.contextPath}/assets/layui.all.js"></script>
<script>
//单个删除
function Del(id){
	if(confirm("您确定要删除吗?")){
		return true;
	}else{
		return false;
	}
}
// 核心：拼接完整的html格式文档并填充数据
//使用outerHTML属性获取整个table元素的HTML代码， 包括根标签<table></table>
// 自定义封装html格式文档<html><head></head><body></body></html>
// 设置字符集，告诉浏览器以utf8方式解析，避免乱码<meta charset='utf-8'/>
// 获取table数据并填充到自定义的html格式文档中
var table_content = document.querySelector("#table_content").outerHTML;
var html = "<html><head><meta charset='utf-8' /></head><body>" + table_content + "</body></html>";

// 实例化一个Blob对象，
// param1：包含文件内容的数组，
// param2：包含文件类型属性的对象
var blob = new Blob([html], {
    type: "application/vnd.ms-excel"
});
var output_table = document.querySelector("#output_table");
// 利用URL.createObjectURL()方法为a元素生成blob URL
output_table.href = URL.createObjectURL(blob);
// 设置文件名，低级浏览器不支持
output_table.download = "学生入住信息表.xls";


</script>


</body>
</html>