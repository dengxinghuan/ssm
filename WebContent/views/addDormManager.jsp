<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>新增宿舍管理员</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/view.css"/>
</head>
<body>
<div class="layui-content">
    <div class="layui-row">
        <div class="layui-card">
            <div class="layui-card-header">添加宿舍管理员</div>
            
            <form class="layui-form layui-card-body" action="${pageContext.request.contextPath}/dormManager/addDormManager" method="post">
                
                
                <div class="layui-form-item">
                    <label class="layui-form-label">账号：</label>
                    <div class="layui-input-block">
                        <input type="text" name="no" required  lay-verify="required" placeholder="请输入账号" autocomplete="off" class="layui-input" style="width:190px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">姓名：</label>
                    <div class="layui-input-block">
                        <input type="text" name="name" required  lay-verify="required" placeholder="请输入姓名" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">性别：</label>
                    <div class="layui-input-block">
                        <input type="radio" name="sex" value="男" title="男" checked>
                        <input type="radio" name="sex" value="女" title="女" >
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">电话：</label>
                    <div class="layui-input-block">
                        <input type="text" name="phone" required  lay-verify="required|phone" placeholder="请输入电话" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn layui-btn-blue" lay-submit lay-filter="formDemo">提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
                
            </form>
            
        </div>
    </div>
</div>
<script  >
function showDept(){  
	//this[selectedIndex].value) type="text/javascript"
	alert("jjjjjhhd");
	//console.log(obj);
  /*   var dept = obj;           
    $.ajax({  
        type: "post",    
        url: "${pageContext.request.contextPath}/drom/findBuildAll",    
        data: "buildId="+dept,  
        async: false,  
        success: function(data){ 
           console.log(data) 
        }     
    }); */
}
</script>
<script src="${pageContext.request.contextPath}/assets/layui.all.js"></script>
<script>
    var form = layui.form
            ,layer = layui.layer;
</script>
<script src="${pageContext.request.contextPath}/assets/jquery-3.4.1.js"></script>

</body>
</html>