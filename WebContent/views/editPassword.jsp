<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <title>修改密码</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/view.css"/>
</head>
<body>

<script type="text/javascript">
function checkForm(){
	var oldPassword=document.getElementById("oldPassword").value;
	var newPassword=document.getElementById("newPassword").value;
	var rPassword=document.getElementById("rPassword").value;
	
	 $("#error").html("");
	if(oldPassword==""||newPassword==""||rPassword==""){
		document.getElementById("error").innerHTML="信息填写不完整！";
		return false;
	} else if(newPassword == oldPassword){
		document.getElementById("error").innerHTML="修改前后密码不能一致！";
		return false;
	}else if(newPassword!=rPassword){
		document.getElementById("error").innerHTML="新密码两次填写不一致！";
		return false;
	}
	/* 
	if(onblurFn(oldPassword)){
		return false;
	}
	 */
	window.location.href="password.action?action=change&newPassword="+newPassword;
}


function onblurFn(oldPassWord){
	$("#error").html(""); 
	if(oldPassWord != null && oldPassWord != ""){
		//校验用户输入的原密码正确
		$.ajax({
			   type: "POST",
			   url: "${pageContext.request.contextPath}/password.action?action=ajaxOldPassWord",
			   data: "oldPassWord="+oldPassWord,
			   async:false,//同步请求
			   success: function(msg){
			     if(msg){
			    	 //用户输入的原密码不正确
			    	 document.getElementById("oldPassword").value="";
			    	$("#error").html(msg); 
			    	return true;
			     }else{
			    	 return false;
			     }
			   }
			});
	}else{
		document.getElementById("error").innerHTML="请输入原密码！";
		return false;
	}
}
 
	
	$(document).ready(function(){
		$("ul li:eq(5)").addClass("active");
	});
</script>

<div class="layui-content">
    <div class="layui-row">
        <div class="layui-card">
            <div class="layui-card-header">修改密码</div>
            
            <form class="layui-form layui-card-body" action="${pageContext.request.contextPath}/PasswordServlet" method="post">

                
                
                <div class="layui-form-item">
                    <label class="layui-form-label">原密码：</label>
                    <div class="layui-input-block">
                        <input type="password" id="oldPassword"  name="oldPassword" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">新密码：</label>
                    <div class="layui-input-block">
                        <input type="password" id="newPassword"  name="newPassword" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>
                
                <div class="layui-form-item">
                    <label class="layui-form-label">确认密码</label>
                    <div class="layui-input-block">
                        <input type="password" id="rPassword"  name="rPassword" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input " style="width:190px">
                    </div>
                </div>


                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="button" class="layui-btn layui-btn-blue" lay-submit lay-filter="formDemo" onclick="checkForm()">提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                    
                    <div align="center">
						<font id="error" color="red"></font>
					</div>
                    
                </div>
            </form>
            
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/assets/layui.all.js"></script>
<script>
    var form = layui.form
            ,layer = layui.layer;
</script>
<script src="${pageContext.request.contextPath}/assets/jquery-3.4.1.js"></script>
</body>
</html>