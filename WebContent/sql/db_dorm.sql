/*
Navicat MySQL Data Transfer

Source Server         : ww
Source Server Version : 50625
Source Host           : localhost:3306
Source Database       : db_dorm

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2022-06-13 14:05:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dorm
-- ----------------------------
DROP TABLE IF EXISTS `dorm`;
CREATE TABLE `dorm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dorm_no` varchar(20) NOT NULL COMMENT '宿舍号',
  `remark` varchar(50) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL COMMENT '宿舍楼',
  `max_num` int(11) DEFAULT '0' COMMENT '最大人数',
  `dorm_status` varchar(50) DEFAULT NULL COMMENT '状态',
  `count` int(11) DEFAULT NULL COMMENT '多少人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `dorm_weiyi` (`build_id`,`dorm_no`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of dorm
-- ----------------------------
INSERT INTO `dorm` VALUES ('1', '101', '备注', '1', '4', '已满', '4');
INSERT INTO `dorm` VALUES ('2', '102', '备注', '1', '4', '未满', '1');
INSERT INTO `dorm` VALUES ('3', '103', '备注', '2', '4', '未满', '0');
INSERT INTO `dorm` VALUES ('4', '104', '备注', '2', '4', '未满', '0');
INSERT INTO `dorm` VALUES ('5', '105', '备注', '3', '4', '未满', '0');

-- ----------------------------
-- Table structure for record
-- ----------------------------
DROP TABLE IF EXISTS `record`;
CREATE TABLE `record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  `dorm_no` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `detail` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of record
-- ----------------------------
INSERT INTO `record` VALUES ('38', '202204', '邓行焕', '1', '102', '2022-06-13', '请假');
INSERT INTO `record` VALUES ('40', '202204', '邓行焕', '1', '102', '2022-06-13', '晚归');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `no` varchar(20) DEFAULT NULL,
  `dorm_no` varchar(20) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  `sex` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '', 'admin', null, null, '1', '12345678910', '0');
INSERT INTO `user` VALUES ('3', '宿管李四', '123456', 'sg1', null, null, '男', '12345678910', '1');
INSERT INTO `user` VALUES ('4', '卫庄', '123456', '202201', '101', '1', '男', '19301302043', '2');
INSERT INTO `user` VALUES ('6', '盖聂', '123456', '202202', '101', '1', '男', '12345678910', '2');
INSERT INTO `user` VALUES ('41', '大幅度', '123456', '202203', '101', '1', '男', '18311212121', '2');
INSERT INTO `user` VALUES ('49', '邓行焕', '123456', '202204', '102', '1', '男', '18377412257', '2');
INSERT INTO `user` VALUES ('59', '宿管李红', '123456', 'sg2', null, null, '女', '18377546623', '1');
INSERT INTO `user` VALUES ('61', '冻干粉', '123456', '202205', '101', '1', '男', '18377452215', '2');
