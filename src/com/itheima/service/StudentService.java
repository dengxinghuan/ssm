package com.itheima.service;



import java.util.List;

import com.itheima.domain.Dorm;
import com.itheima.domain.User;


public interface StudentService {
	//查询全部学生
	List<User> findStudentAll();
	
	//根据Id查询学生
	User findStudentId(int id);
	
	//多条件查询
	List<User> findStudentMany(User user);
	
	//添加学生
	Integer addStudent(User user, Dorm dorm);
	
	//修改学生信息
	Integer editStudent(User user,Dorm dorm);
	
	//删除学生信息
	Integer delStudent(int id,Dorm dorm);
	
	//修改学生宿舍号信息时，修改前宿舍已住人数-1，
	
	//根据学号查询学生，判断添加缺勤记录时学生是不是存在
	User findStudentNo(User user);
	
}
