package com.itheima.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itheima.domain.User;
import com.itheima.mapper.DormManagerMapper;
import com.itheima.service.DormManagerService;
@Service
@Transactional
public class DormManagerServiceImpl implements DormManagerService {
	//业务逻辑层中一定有访问层的的对象
		@Autowired
	private DormManagerMapper dormManagerMapper;

		//查询全部宿舍管理员
		@Override
		public List<User> findDormManagerAll() {
			// TODO Auto-generated method stub
			return dormManagerMapper.findDormManagerAll();
		}

		//添加宿舍管理员
		@Override
		public Integer addDormManager(User user) {
			// TODO Auto-generated method stub
			return dormManagerMapper.addDormManager(user);
		}

		//多条件查询
		@Override
		public List<User> findDormManagerMany(User user) {
			// TODO Auto-generated method stub
			return dormManagerMapper.findDormManagerMany(user);
		}

		//删除宿舍管理员
		@Override
		public Integer delDormManager(int id) {
			// TODO Auto-generated method stub
			return dormManagerMapper.delDormManager(id);
		}
		
		
}
