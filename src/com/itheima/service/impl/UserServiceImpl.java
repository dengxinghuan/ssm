package com.itheima.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itheima.domain.User;
import com.itheima.mapper.UserMapper;
import com.itheima.service.UserService;

/**
 *用户接口实现类
 */
@Service
public class UserServiceImpl implements UserService {
	//注入userMapper
	@Autowired
	private UserMapper userMapper;
	//通过User的用户账号和用户密码查询用户信息
	@Override
	public User login(User user) {
		return userMapper.login(user);
	}
	@Override
	public User findUserAll(User user) {
		// TODO Auto-generated method stub
		return userMapper.findUserAll(user);
	}
	
	//查询全部用户
	
}
