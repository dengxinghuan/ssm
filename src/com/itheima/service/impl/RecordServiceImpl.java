package com.itheima.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itheima.domain.Record;
import com.itheima.mapper.RecordMapper;
import com.itheima.service.RecordService;

@Service
@Transactional
public class RecordServiceImpl implements RecordService {
	//业务逻辑层中一定有访问层的的对象
	@Autowired
	private RecordMapper recordMapper;

	//查询缺勤记录
	@Override
	public List<Record> findRecordAll() {
		// TODO Auto-generated method stub
		return recordMapper.findRecoreAll();
	}

	//按名字查询缺勤记录
	@Override
	public List<Record> findRecordName(Record record) {
		// TODO Auto-generated method stub
		return recordMapper.findRecordName(record);
	}

	//添加缺勤记录
	@Override
	public Integer addRecord(Record record) {
		// TODO Auto-generated method stub
		return recordMapper.addRecord(record);
	}

	//删除缺勤记录（销假）
	@Override
	public Integer delRecord(int id) {
		// TODO Auto-generated method stub
		return recordMapper.delRecord(id);
	}
	
	
	
	
	
}
