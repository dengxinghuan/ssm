package com.itheima.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.domain.Dorm;
import com.itheima.domain.User;
import com.itheima.mapper.DormMapper;
import com.itheima.mapper.StudentMapper;

import com.itheima.service.StudentService;

import entity.PageResult;
@Service
@Transactional
public class StudentServiceImpl implements StudentService {
	//业务逻辑层中一定有访问层的的对象
	@Autowired
	private StudentMapper studentMapper;
	
	@Autowired
	private DormMapper dormMapper;
	
	//查询全部学生
	@Override
	public List<User> findStudentAll() {
	   
		return studentMapper.findStudentAll();
	}
	
	//根据Id查询学生
	@Override
	public User findStudentId(int id) {
		// TODO Auto-generated method stub
		return studentMapper.findStudentId(id);
	}
	
	
	//多条件查询
	@Override
	public List<User> findStudentMany(User user) {
		// TODO Auto-generated method stub
		return studentMapper.findStudentMany(user);
	}

	//添加学生
	@Override
	public Integer addStudent(User user,Dorm dorm) {
		// TODO Auto-generated method stub
		Integer addStudent = studentMapper.addStudent(user);
		Integer editNum = dormMapper.editNumAdd(dorm);
		
		return addStudent + editNum;
	}

	//修改学生信息
	@Override
	public Integer editStudent(User user,Dorm dorm) {
		// TODO Auto-generated method stub
		Integer editstu = studentMapper.editStudent(user);
		Integer editNumadd = dormMapper.editNumAdd(dorm);
		return editstu+editNumadd;
	}

	//删除学生
	@Override
	public Integer delStudent(int id,Dorm dorm) {
		// TODO Auto-generated method stub
		Integer delStudent=studentMapper.delStudent(id);
		Integer editNum=dormMapper.editNumRed(dorm);
		return delStudent+editNum;
	}

	//修改学生宿舍号信息时，修改前宿舍已住人数-1，
	
	
	//根据学号查询学生，判断添加缺勤记录时学生是不是存在
	@Override
	public User findStudentNo(User user) {
		// TODO Auto-generated method stub
		return studentMapper.findStudentNo(user);
	}

	
	
	
	

	
   }

