package com.itheima.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itheima.domain.Dorm;
import com.itheima.mapper.DormMapper;
import com.itheima.service.DormService;
@Service
@Transactional
public class DormServiceImpl implements DormService{
	//业务逻辑层中一定有访问层的的对象
		@Autowired
		private DormMapper dormMapper;

		//查询宿舍
		@Override
		public List<Dorm> findDormAll() {
			// TODO Auto-generated method stub
			return dormMapper.findDormAll();
		}
		
		//根据ID查询宿舍
		@Override
		public Dorm findDormId(int id) {
			// TODO Auto-generated method stub
			return dormMapper.findDormId(id);
		}
		

		//多条件查询
		@Override
		public List<Dorm> findDormMany(Dorm dorm) {
			// TODO Auto-generated method stub
			return dormMapper.findDormMany(dorm);
		}

		//添加宿舍
		@Override
		public Integer addDorm(Dorm dorm) {
			// TODO Auto-generated method stub
			return dormMapper.addDorm(dorm);
		}

		//修改宿舍
		@Override
		public Integer editDorm(Dorm dorm) {
			// TODO Auto-generated method stub
			return dormMapper.editDorm(dorm);
		}

		//查找所有宿舍楼
		@Override
		public List<Dorm> findBuildAll() {
			// TODO Auto-generated method stub
			return dormMapper.findBuildAll();
		}

		//查找所有宿舍楼对应的宿舍号
		@Override
		public List<Dorm> findDorm_no(int build_id) {
			// TODO Auto-generated method stub
			System.out.println(build_id + "service....");
			return dormMapper.findDorm_no(build_id);
		}

		@Override
		public Integer editNum(Dorm dorm) {
			// TODO Auto-generated method stub
			return null;
		}

		//根据宿舍楼查询宿舍
		@Override
		public Integer fingDormBuild(Dorm dorm) {
			// TODO Auto-generated method stub
			return dormMapper.fingDormBuild(dorm);
		}

		//修改宿舍状态
		@Override
		public Integer editdormStatus(Dorm dorm) {
			// TODO Auto-generated method stub
			return dormMapper.editdormStatus(dorm);
		}

		//查询宿舍床位数和已住人数
		@Override
		public Dorm findDormNum(Dorm dorm) {
			// TODO Auto-generated method stub
			return dormMapper.findDormNum(dorm);
		}

		
		//删除宿舍
		@Override
		public Integer delDorm(int id) {
			// TODO Auto-generated method stub
			return dormMapper.delDorm(id);
		}

	
		
		
		
		
}
