package com.itheima.service;

import java.util.List;

import com.itheima.domain.Record;

public interface RecordService {
	//查询缺勤记录
	List<Record> findRecordAll();
	
	//按名字查询缺勤记录
	List<Record> findRecordName(Record record);

	//添加缺勤记录
	Integer addRecord(Record record);
	
	//删除缺勤记录（销假）
	Integer delRecord(int id);
}
