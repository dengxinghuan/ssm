package com.itheima.service;

import java.util.List;

import com.itheima.domain.User;

public interface DormManagerService {

	//查询全部宿管
	List<User> findDormManagerAll();
	
	//添加宿舍管理员
	Integer addDormManager(User user);
	
	//多条件查询
	List<User> findDormManagerMany(User user);
	
	//删除宿舍管理员
	Integer delDormManager(int id);
}
