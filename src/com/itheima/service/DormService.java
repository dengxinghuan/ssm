package com.itheima.service;

import java.util.List;

import com.itheima.domain.Dorm;

public interface DormService {
	//查询全部宿舍
	List<Dorm> findDormAll();
	
	//根据ID查询宿舍
	Dorm findDormId(int id);
	
	//多条件查询
	List<Dorm> findDormMany(Dorm dorm);
	
	//添加宿舍
	Integer addDorm(Dorm dorm);
	
	//修改宿舍信息
	Integer editDorm(Dorm dorm);
	

	//查找所有宿舍楼对应的宿舍号
	public List<Dorm> findBuildAll();
	
	//查询宿舍楼对应的宿舍号
	List<Dorm> findDorm_no(int build_id);
	
	//修改宿舍人数
	Integer editNum(Dorm dorm);
	
	//根据宿舍楼查询宿舍
	Integer fingDormBuild(Dorm dorm);
	
	//修改宿舍状态
	Integer editdormStatus(Dorm dorm);
	
	//查询宿舍床位数和已住人数
	Dorm findDormNum(Dorm dorm);
	
	
	//删除宿舍
	Integer delDorm(int id);

	
	
}
