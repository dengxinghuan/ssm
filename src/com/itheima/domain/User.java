package com.itheima.domain;



public class User {
	private Integer id;//id
	private String name;//名字
	private String password;//密码
	private String no;//账号—学会说呢过一般是学号 用作查缺勤
	private String dorm_no;//宿舍号
	private Integer build_id;//宿舍楼
	private String sex;//性别
	private String phone;//电话
	private Integer role_id;//角色 0超级管理 1宿管阿姨 2 学生
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getDorm_no() {
		return dorm_no;
	}
	public void setDorm_no(String dorm_no) {
		this.dorm_no = dorm_no;
	}
	public Integer getBuild_id() {
		return build_id;
	}
	public void setBuild_id(Integer build_id) {
		this.build_id = build_id;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getRole_id() {
		return role_id;
	}
	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", password=" + password + ", no=" + no + ", dorm_no=" + dorm_no
				+ ", build_id=" + build_id + ", sex=" + sex + ", phone=" + phone + ", role_id=" + role_id + "]";
	}
	
	//@Override
//	public Integer hashCode() {
//		final Integer prime = 31;
//		Integer result = 1;
//		result = prime * result + build_id;
//		result = prime * result + ((dorm_no == null) ? 0 : dorm_no.hashCode());
//		result = prime * result + id;
//		result = prime * result + ((name == null) ? 0 : name.hashCode());
//		result = prime * result + ((no == null) ? 0 : no.hashCode());
//		result = prime * result + ((password == null) ? 0 : password.hashCode());
//		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
//		result = prime * result + role_id;
//		result = prime * result + ((sex == null) ? 0 : sex.hashCode());
//		return result;
//	}
	//判断对象是否相等
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (build_id != other.build_id)
			return false;
		if (dorm_no == null) {
			if (other.dorm_no != null)
				return false;
		} else if (!dorm_no.equals(other.dorm_no))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (no == null) {
			if (other.no != null)
				return false;
		} else if (!no.equals(other.no))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (role_id != other.role_id)
			return false;
		if (sex == null) {
			if (other.sex != null)
				return false;
		} else if (!sex.equals(other.sex))
			return false;
		return true;
	}
	
	
}
