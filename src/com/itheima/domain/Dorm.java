package com.itheima.domain;


public class Dorm {
	private Integer id;//宿舍id
	private String dorm_no;//宿舍号
	private Integer build_id;//宿舍楼
	private Integer max_num;//最大人数
	private String dorm_status;//宿舍状态
	private Integer count;//住了多少人
	 
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDorm_no() {
		return dorm_no;
	}

	public void setDorm_no(String dorm_no) {
		this.dorm_no = dorm_no;
	}

	public Integer getBuild_id() {
		return build_id;
	}

	public void setBuild_id(Integer build_id) {
		this.build_id = build_id;
	}

	public Integer getMax_num() {
		return max_num;
	}

	public void setMax_num(Integer max_num) {
		this.max_num = max_num;
	}

	public String getDorm_status() {
		return dorm_status;
	}

	public void setDorm_status(String dorm_status) {
		this.dorm_status = dorm_status;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	//	@Override
//	public Integer hashCode() {
//		final Integer prime = 31;
//		Integer result = 1;
//		result = prime * result + build_id;
//		result = prime * result + count;
//		result = prime * result + ((dorm_no == null) ? 0 : dorm_no.hashCode());
//		result = prime * result + ((dorm_status == null) ? 0 : dorm_status.hashCode());
//		result = prime * result + id;
//		result = prime * result + max_num;
//		return result;
//	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dorm other = (Dorm) obj;
		if (build_id != other.build_id)
			return false;
		if (count != other.count)
			return false;
		if (dorm_no == null) {
			if (other.dorm_no != null)
				return false;
		} else if (!dorm_no.equals(other.dorm_no))
			return false;
		if (dorm_status == null) {
			if (other.dorm_status != null)
				return false;
		} else if (!dorm_status.equals(other.dorm_status))
			return false;
		if (id != other.id)
			return false;
		if (max_num != other.max_num)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Dorm [id=" + id + ", dorm_no=" + dorm_no + ", build_id=" + build_id + ", max_num=" + max_num
				+ ", dorm_status=" + dorm_status + ", count=" + count + "]";
	}

	
	
	
	
	
	
	
	
}
