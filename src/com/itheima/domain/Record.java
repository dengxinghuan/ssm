package com.itheima.domain;

public class Record {
	private int id;//记录id
	private String no;//账号—学会说呢过一般是学号 用作查缺勤
	private String name;//名字
	private int build_id;//宿舍楼
	private String dorm_no;//宿舍号
	private String date;//日期
	private String detail;//备注
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBuild_id() {
		return build_id;
	}
	public void setBuild_id(int build_id) {
		this.build_id = build_id;
	}
	public String getDorm_no() {
		return dorm_no;
	}
	public void setDorm_no(String dorm_no) {
		this.dorm_no = dorm_no;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	@Override
	public String toString() {
		return "Record [id=" + id + ", no=" + no + ", name=" + name + ", build_id=" + build_id + ", dorm_no=" + dorm_no
				+ ", date=" + date + ", detail=" + detail + "]";
	}
	
	
	
}
