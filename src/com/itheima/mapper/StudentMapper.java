package com.itheima.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.github.pagehelper.Page;

import com.itheima.domain.User;


public interface StudentMapper {
	//查询全部学生
	@Select("select * from user where role_id='2' order by build_id")
	@Results(id="studentMap",value= {
			//id字段默认为false，表示不是主键
            //column表示数据库表字段，property表示实体类属性名。
    		//column的值注入到User属性值
            @Result(id = true,column = "id",property = "id"),//id
            @Result(column = "name",property = "name"),//名字
            @Result(column = "password",property = "password"),//密码
            @Result(column = "no",property = "no"),//账号
            @Result(column = "dorm_no",property = "dorm_no"),//宿舍号
            @Result(column = "build_id",property = "build_id"), //宿舍楼
            @Result(column = "sex",property = "sex"),//性别
            @Result(column = "phone",property = "phone"),//电话
            @Result(column = "role_id",property = "role_id")//角色 0超级管理 1宿管阿姨 2 学生
	})
    public List<User> findStudentAll();
	
	//根据Id查询学生
	@Select("SELECT * FROM user where id=#{id}")
	@ResultMap("studentMap")
	User findStudentId(int id);
	
	
	//多条件查询
	@Select({"<script>" +
            "SELECT * FROM user " +
            "where role_id ='2'" +
            "<if test=\"no != null\"> AND  no  like  CONCAT('%',#{no},'%')</if>" +
            "<if test=\"name != null\"> AND name like  CONCAT('%', #{name},'%') </if>" +
            "<if test=\"sex != null\"> AND sex like  CONCAT('%', #{sex},'%')</if>" +
            "<if test=\"build_id != null\"> AND build_id like  CONCAT('%', #{build_id},'%')</if>" +
            "<if test=\"dorm_no != null\"> AND dorm_no like  CONCAT('%', #{dorm_no},'%')</if>" +
            "order by build_id" +
            "</script>"
    })
    @ResultMap("studentMap")//实体类属性名和数据库字段不一样要写，否则可以不写
	public List<User> findStudentMany(User user);
	
	
	//插入添加学生
	@Insert({"insert into user (id,name,password,no,dorm_no,build_id,sex,phone,role_id)" +
	"values (#{id},#{name},123456,#{no},#{dorm_no},#{build_id},#{sex},#{phone},2)"
	})
	@ResultMap("studentMap")
	Integer addStudent(User user);
	
	//修改学生信息
	@Update("update user set id=#{id},name=#{name},no=#{no},dorm_no=#{dorm_no},build_id=#{build_id},sex=#{sex},phone=#{phone} where id=#{id}")
	@ResultMap("studentMap")
	Integer editStudent(User user);
	
	//删除学生信息
	@Delete("delete from user where id=#{id}")
	@ResultMap("studentMap")
	Integer delStudent(int id);
	
	//根据学号查询学生，判断添加缺勤记录时学生是不是存在
	@Select({"<script>" +"SELECT * FROM user where no=#{no}"+
			"<if test=\"name != null\"> AND  name= #{name} </if>" +
			"</script>"
			})
	@ResultMap("studentMap")
	User findStudentNo(User user);
}
