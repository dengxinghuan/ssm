package com.itheima.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.itheima.domain.Dorm;

public interface DormMapper {
	@Select("select * from dorm order by build_id")
	@Results(id="dormMap",value= {
			//id字段默认为false，表示不是主键
            //column表示数据库表字段，property表示实体类属性名。
    		//column的值注入到User属性值
		@Result(id = true,column = "id",property = "id"),//id
	    @Result(column = "dorm_no",property = "dorm_no"),//宿舍号
	    @Result(column = "build_id",property = "build_id"),//宿舍楼
	    @Result(column = "max_num",property = "max_num"),//最大人数
	    @Result(column = "dorm_status",property = "dorm_status"),//宿舍状态
	    @Result(column = "count",property = "count")//已住人数
	})
	//查询宿舍
	public List<Dorm> findDormAll();
	
	//根据Id查询宿舍
	@Select("SELECT * FROM dorm where id=#{id}")
	@ResultMap("dormMap")
	Dorm findDormId(int id);
	
	//多条件查询宿舍
	@Select({"<script>" +
			"SELECT * FROM dorm " +
			"where 1=1"+
			"<if test=\"dorm_no != null\"> AND  dorm_no  like  CONCAT('%',#{dorm_no},'%')</if>" +
			"<if test=\"dorm_status != null\"> AND  dorm_status  like  CONCAT('%',#{dorm_status},'%')</if>" +
			"order by build_id" +
			"</script>"
	})
	@ResultMap("dormMap")
	//多条件查询
	public List<Dorm> findDormMany(Dorm dorm);
	
	
	//插入添加宿舍
	@Insert({"insert into dorm (id,dorm_no,build_id,max_num,dorm_status,count)" + 
			"values (#{id},#{dorm_no},#{build_id},#{max_num},#{dorm_status},#{count})"
	})
	@ResultMap("dormMap")
	Integer addDorm(Dorm dorm);
	
	
	//修改宿舍信息
	@Update("update dorm set id=#{id},dorm_no=#{dorm_no},build_id=#{build_id},max_num=#{max_num},dorm_status=#{dorm_status},count=#{count} where id=#{id}")
	@ResultMap("dormMap")
	Integer editDorm(Dorm dorm);

	
	//查找所有宿舍楼
	@Select("SELECT DISTINCT build_id FROM dorm")
	@ResultMap("dormMap")
	public List<Dorm> findBuildAll();
	
	//查找所有宿舍楼对应的宿舍号
	@Select({"<script>" +"SELECT * FROM dorm where 1=1" +
			"<if test=\"build_id != null\"> AND  build_id= #{build_id} </if>" +
			"</script>"
			})
	@ResultMap("dormMap")
	List<Dorm> findDorm_no(@Param("build_id")int build_id);
	
	
	//修改宿舍人数+1
	@Update("update dorm set count =count+1 where dorm_no = #{dorm_no}")
	@ResultMap("dormMap")
	Integer editNumAdd(Dorm dorm);
	
	
	//修改宿舍人数-1
	@Update("update dorm set count =count-1 where dorm_no =#{dorm_no}")
	@ResultMap("dormMap")
	Integer editNumRed(Dorm dorm);
	
	
	//修改宿舍状态（已满,未满）
	@Update("update dorm set dorm_status='已满'  where dorm_no=#{dorm_no}")
	@ResultMap("dormMap")
	Integer editdormStatus(Dorm dorm);
	
	
	//根据宿舍楼查询宿舍
	@Select({"<script>" +"SELECT count(*) FROM dorm where dorm_no=#{dorm_no}" +
			"<if test=\"build_id != null\"> AND  build_id= #{build_id} </if>" +
			"</script>"
			})
	//@ResultMap("dormMap")
	Integer fingDormBuild(Dorm dorm);
	
	
	//查询床位数和已住人数
	@Select({"<script>" +"SELECT * FROM dorm where dorm_no=#{dorm_no}" +
			"<if test=\"build_id != null\"> AND  build_id= #{build_id} </if>" +
			"</script>"
			})
	@ResultMap("dormMap")
	Dorm findDormNum(Dorm dorm);
	
	
	//修改已住人数-1
	
	
	
	//删除宿舍
	@Delete("delete from dorm where id=#{id}")
	@ResultMap("dormMap")
	Integer delDorm(int id);
	
	
	
	//多条件查询宿舍号
//	@Select({"<script>" + 
//			"SELECT * FROM dorm where 1=1" +
//			"<if test=\"build_id != null\"> AND  build_id  like  CONCAT('%',#{build_id},'%')</if>" +
//			"<if test=\"dorm_no != null\"> AND  dorm_no  like  CONCAT('%',#{dorm_no},'%')</if>" +
//			"<if test=\"dorm_status != null\"> AND  dorm_status  like  CONCAT('%',#{dorm_status},'%')</if>" +
//			"</script>"
//	})
//	@ResultMap("dormMap")
//	public List<Dorm> findDormNoStatus(String dorm_no,String build_id);
	
}
