package com.itheima.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.itheima.domain.User;

public interface UserMapper {
	@Select("select * from user where no=#{no} AND password=#{password}")
	@Results(id="userMap",value= {
			//id字段默认为false，表示不是主键
            //column表示数据库表字段，property表示实体类属性名。
    		//column的值注入到User属性值
            @Result(id = true,column = "id",property = "id"),
            @Result(column = "name",property = "name"),
            @Result(column = "password",property = "password"),
            @Result(column = "no",property = "no"),
            @Result(column = "dorm_no",property = "dorm_no"),
            @Result(column = "build_id",property = "build_id"), 
            @Result(column = "sex",property = "sex"),
            @Result(column = "phone",property = "phone"),
            @Result(column = "role_id",property = "role_id")
	})
	User login(User user);
	
	//查询全部用户
	@Select("select * from user where no=#{no}")
	@ResultMap("userMap")//实体类属性名和数据库字段不一样要写，否则可以不写
	User findUserAll(User user);
}
