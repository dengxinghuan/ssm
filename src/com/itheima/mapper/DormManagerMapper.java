package com.itheima.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.itheima.domain.User;

public interface DormManagerMapper {

	//查询全部宿管
	@Select("select * from user where role_id='1' order by no")
	@Results(id="DormManagerMap",value= {
			//id字段默认为false，表示不是主键
            //column表示数据库表字段，property表示实体类属性名。
    		//column的值注入到User属性值
            @Result(id = true,column = "id",property = "id"),//id
            @Result(column = "name",property = "name"),//名字
            @Result(column = "password",property = "password"),//密码
            @Result(column = "no",property = "no"),//账号
            @Result(column = "dorm_no",property = "dorm_no"),//宿舍号
            @Result(column = "build_id",property = "build_id"), //宿舍楼
            @Result(column = "sex",property = "sex"),//性别
            @Result(column = "phone",property = "phone"),//电话
            @Result(column = "role_id",property = "role_id")//角色 0超级管理 1宿管阿姨 2 学生
	})
    public List<User> findDormManagerAll();
	
	//添加宿舍管理员
	@Insert({"insert into user (id,name,password,no,dorm_no,build_id,sex,phone,role_id)" +
			"values (#{id},#{name},123456,#{no},#{dorm_no},#{build_id},#{sex},#{phone},1)"
			})
	@ResultMap("DormManagerMap")
	Integer addDormManager(User user);
	
	//多条件查询
	@Select({"<script>" +
            "SELECT * FROM user " +
            "where role_id ='1'" +
            "<if test=\"no != null\"> AND  no  like  CONCAT('%',#{no},'%')</if>" +
            "<if test=\"name != null\"> AND name like  CONCAT('%', #{name},'%') </if>" +
            "<if test=\"sex != null\"> AND sex like  CONCAT('%', #{sex},'%')</if>" +
            "<if test=\"build_id != null\"> AND build_id like  CONCAT('%', #{build_id},'%')</if>" +
            "<if test=\"dorm_no != null\"> AND dorm_no like  CONCAT('%', #{dorm_no},'%')</if>" +
            "order by build_id" +
            "</script>"
    })
	@ResultMap("DormManagerMap")
	public List<User> findDormManagerMany(User user);
	
	//删除宿舍管理员
	@Delete("delete from user where id=#{id}")
	@ResultMap("DormManagerMap")
	Integer delDormManager(int id);
}
