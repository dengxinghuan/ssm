package com.itheima.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.itheima.domain.Record;

public interface RecordMapper {
	//查询缺勤记录
	@Select("select * from record order by date DESC")
	@Results(id="recordMap",value= {
			//id字段默认为false，表示不是主键
            //column表示数据库表字段，property表示实体类属性名。
    		//column的值注入到User属性值
		@Result(id = true,column = "id",property = "id"),//id
		@Result(column = "no",property = "no"),//学号
		@Result(column = "name",property = "name"),//名字
		@Result(column = "build_id",property = "build_id"),//宿舍楼
		@Result(column = "dorm_no",property = "dorm_no"),//宿舍号
		@Result(column = "date",property = "date"),//日期
		@Result(column = "detail",property = "detail")//备注
	})
	public List<Record> findRecoreAll();
	
	
	//按学生名字查询
	@Select({"<script>" +
			"SELECT * FROM record " +
			"where 1=1 " +
			"<if test=\"name != null\"> AND name like  CONCAT('%', #{name},'%') </if>" +
			"order by date DESC" +
			"</script>"
	})
	@ResultMap("recordMap")
	public List<Record> findRecordName(Record record);
	
	//添加缺勤记录
	@Insert({"insert into record (id,no,name,build_id,dorm_no,date,detail)" +
		"values (#{id},#{no},#{name},#{build_id},#{dorm_no},#{date},#{detail})"
	})
	@ResultMap("recordMap")
	Integer addRecord(Record record);
	
	//删除缺勤记录（销假）
	@Delete("delete from record where id=#{id}")
	@ResultMap("recordMap")
	Integer delRecord(int id);
}
