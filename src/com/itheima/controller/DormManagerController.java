package com.itheima.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itheima.domain.User;
import com.itheima.service.DormManagerService;

import entity.Result;

@Controller
@RequestMapping("/dormManager")
public class DormManagerController {

	//注入DormManagerService对象
	@Autowired
	private DormManagerService dormManagerService;
	
	@RequestMapping("/findDormManagerAll")
	public ModelAndView findDormManagerAll() {
		//查询全部宿管
		List<User> list=dormManagerService.findDormManagerAll();
		ModelAndView modelAndView= new ModelAndView();
		//将查询到的数据存放在ModelAndView的对象中
		modelAndView.addObject("list", list);
		modelAndView.setViewName("listdormmanager");
		return modelAndView;
	}
	
	//多条件查询
	@RequestMapping("/findDormManagerMany")
	public ModelAndView findDormManagerMany(User user) {
		List<User> list =dormManagerService.findDormManagerMany(user);
		ModelAndView modelAndView = new ModelAndView();
		// 将查询到的数据存放在 ModelAndView的对象中
		modelAndView.addObject("list", list);
		modelAndView.setViewName("listdormmanager");
		return modelAndView;
	}
	
	
	//添加宿舍管理员
	@ResponseBody
	@RequestMapping("/addDormManager")
	public Result addDormManager(User user) {
		try {
			Integer count =dormManagerService.addDormManager(user);
			if(count < 1) {
				return new Result(false, "新增宿舍管理员失败!");
			}
			return new Result(true, "新增宿舍管理员成功!");
		}catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "新增宿舍管理员失败!");
		}
	}
	
	//删除宿舍管理员
	@ResponseBody
	@RequestMapping("/delDormManager")
	public Result delDormManager(Integer id) {
		try {
			Integer count =dormManagerService.delDormManager(id);
			if(count <1) {
				return new Result(false,"删除宿舍管理员失败");
			}
			return new Result(true,"删除宿舍管理员成功");
		}catch(Exception e) {
			e.printStackTrace();
			return new Result(false,"删除宿舍管理员失败");
		}
	}
}
