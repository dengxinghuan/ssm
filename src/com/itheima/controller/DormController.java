package com.itheima.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itheima.domain.Dorm;
import com.itheima.service.DormService;

import entity.Result;

/*
 * 学生宿舍管理
 */
@Controller
@RequestMapping("/dorm")
public class DormController {
	//注入DormService对象
	@Autowired
	private DormService dormService;
	
	@RequestMapping("/findDormAll")
	public ModelAndView findDormAll(HttpServletRequest request) {
		//查询全部宿舍
		List<Dorm> list=dormService.findDormAll();
		ModelAndView modelAndView =new ModelAndView();
		//将查询到的数据存放ModelAndView的对象中
		modelAndView.addObject("list", list);
		modelAndView.setViewName("listdorm");
		return modelAndView;
	}
	
	//根据ID查询宿舍，回显修改页面
	@RequestMapping("/findDormId")
	public ModelAndView findDormId(int id,HttpServletRequest request) {
		System.out.println("回显信息到修改宿舍页面");
		Dorm dr=dormService.findDormId(id);
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.addObject("dr", dr);
		modelAndView.setViewName("editDorm");
		return modelAndView;
	}
	
	
	@RequestMapping("/findDormMany")
	public ModelAndView findDormMany(Dorm dorm) {
		//多条件查询
		List<Dorm> list = dormService.findDormMany(dorm);
		ModelAndView modelAndView =new ModelAndView();
		//将查询到的数据存放在ModelAndView的对象中
		modelAndView.addObject("list", list);
		modelAndView.setViewName("listdorm");
		return modelAndView;
	}
	
	
	//跳转添加宿舍页面
	@RequestMapping("adddorm")
	public String adddorm() {
		return "addDorm";
	}
	
	//添加宿舍
	@ResponseBody
	@RequestMapping("/addDorm")
	public Result addDorm(Dorm dorm) {
		try {
			Integer count=dormService.addDorm(dorm);
			if(count < 1) {
				return new Result(false,"添加宿舍失败");
			}
			return new Result(true,"添加宿舍成功");
		}catch(Exception e){
			e.printStackTrace();
			return new Result(false,"添加宿舍失败");
			
		}
		
	}
	
	//修改宿舍信息
	@ResponseBody
	@RequestMapping("/editDorm")
	public Result editDorm(Dorm dorm) {
		System.out.println("进来修改宿舍信息！！！！");
		try {
			Integer count=dormService.editDorm(dorm);
			if(count <1) {
				return new Result(false,"修改宿舍信息失败");
			}
			return new Result(true,"修改宿舍信息成功");
		}catch(Exception e) {
			e.printStackTrace();
			return new Result(false,"修改宿舍信息失败");
		}
	}
	
	//查找所有宿舍楼
	@RequestMapping("/findBuildAll")
	public ModelAndView findBuildAll(HttpServletRequest request, Integer builderId) {
		System.out.println("！！！！！！！！！！！！！！！！！进入了findBuildAll");
		List<Dorm> list =dormService.findBuildAll();
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.addObject("list", list);
		List<Dorm> dorm_nos = null;
		System.out.println(builderId);
		
		if(builderId!=null) {
			modelAndView.addObject("builderId", builderId);			
			dorm_nos = dormService.findDorm_no(builderId);
		}else {
			modelAndView.addObject("builderId", list.get(0).getBuild_id());			
			dorm_nos = dormService.findDorm_no(list.get(0).getBuild_id());
		}
	
		modelAndView.addObject("dorm_nos", dorm_nos);
		modelAndView.setViewName("addStu");
		return modelAndView;
		
	}
	
	
	//查找所有宿舍楼对应的宿舍号
	@RequestMapping("/findDorm_no")
	public ModelAndView findDorm_no(Integer build_id,HttpServletRequest request) {
		System.out.println("！！！！！！！！！！！！！！！！！进入了findDorm_no");
		List<Dorm> list=dormService.findDorm_no(build_id);
		ModelAndView modelAndView =new ModelAndView();
		modelAndView.addObject("list", list);
		modelAndView.setViewName("addStu");
		return modelAndView;
	}
	
	//删除宿舍
	@ResponseBody
	@RequestMapping("/delDorm")
	public Result delDorm(Integer id,Dorm dorm) {
		System.out.println("进来删除宿舍！！！！！！！");
		
		try {
			Integer count=dormService.delDorm(id);
			if(count!=1) {
				return new Result(false,"删除宿舍失败");
			}
			return new Result(true,"删除宿舍成功");
		}catch(Exception e) {
			e.printStackTrace();
			return new Result(false,"删除宿舍失败");
		}
	}
	
	
}
