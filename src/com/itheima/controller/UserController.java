package com.itheima.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.itheima.domain.User;
import com.itheima.service.UserService;

@Controller
public class UserController {
	//注入userService
	@Autowired
	private UserService userService;
	
	
	//用户登录
	@RequestMapping("login")
	public String login(User user,HttpServletRequest request) {
		try {
			User u=userService.login(user);
			//用户账号和密码是否查询出用户信息
			//是：将用户信息存入Session，并跳转到后台首页
			//否：Request域中添加提示信息，并转发到登录页面
			if(u!=null) {
				User s=userService.findUserAll(user);
				System.out.println(s.getRole_id());
				if(s.getRole_id()==0) {
					request.getSession().setAttribute("USER_SESSION", u);
					return "redirect:/index.jsp";//redirect:重定向
				}else if(s.getRole_id()==1) {
					request.getSession().setAttribute("USER_SESSION", u);
					return "redirect:/index.jsp";//redirect:重定向
				}else {
					request.setAttribute("message", "非管理员不能进入后台");
					return "forward:/login.jsp";//forward:转发
				}
			}
			request.setAttribute("message", "用户或密码错误");
			return "forward:/login.jsp";//forward:转发
		}catch(Exception e) {
			e.printStackTrace();
			request.setAttribute("message", "系统错误");
			return "forward:/login.jsp";
		}
	}
	
	//用户注销
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		try {
			HttpSession session=request.getSession();//获取当前会话对象
			session.invalidate();
			return "forward:/login.jsp";
		}catch(Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "系统错误");
			return "forward:/login.jsp";
		}
	}
	
	//跳转修改密码
	@RequestMapping("editpassword")
	public String editpassword() {
		return "editPassword";
	}
}
