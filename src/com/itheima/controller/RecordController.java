package com.itheima.controller;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itheima.domain.Record;
import com.itheima.domain.User;
import com.itheima.service.RecordService;
import com.itheima.service.StudentService;

import entity.Result;

@Controller
@RequestMapping("/record")
public class RecordController {
	//注入RecordService对象
	@Autowired
	private RecordService recordService;
	//注入StudentService对象
	@Autowired
	private StudentService studentService;
	
	@RequestMapping("/findRecordAll")
	public ModelAndView findRecordAll() {
		//查询全部缺勤记录
		List<Record> list=recordService.findRecordAll();
		ModelAndView modelAndView = new ModelAndView();
		//将查询到的数据存放在ModelAndView的对象中
		modelAndView.addObject("list", list);
		modelAndView.setViewName("listrecord");
		return modelAndView;
	}
	
	//按名字查询缺勤记录
	@RequestMapping("/findRecordName")
	public ModelAndView findRecordName(Record record) {
		List<Record> list=recordService.findRecordName(record);
		ModelAndView modelAndView=new ModelAndView();
		//将查询的数据存放在ModelAndView的对象中
		modelAndView.addObject("list", list);
		modelAndView.setViewName("listrecord");
		// 将查询的参数返回到页面，用于回显到查询的输入框中
		modelAndView.addObject("findRecordName", record);
		return modelAndView;
	}
	
	
	//添加缺勤记录
	@ResponseBody
	@RequestMapping("/addRecord")
	public Result addRecord(Record record,User user) {
		
		System.out.println("进来添加缺勤记录！！！");
		try {
			
			User s=studentService.findStudentNo(user);
			System.out.println(s);
			if(s==null) {
				return new Result(false,"信息库没有该学生信息");
			}
			
			Integer count=recordService.addRecord(record);
			if(count<1) {
				return new Result(false,"添加缺勤记录失败");
			}
			return new Result(true,"添加缺勤记录成功");
		}catch(Exception e){
			e.printStackTrace();
			return new Result(false,"添加缺勤记录失败");
			
		}
	}
	
	//删除缺勤记录
//	@ResponseBody
//	@RequestMapping("/delRecord")
//	public Result delRecord(Integer id) {
//		System.out.println("进来删除缺勤记录！！！");
//		try {
//			Integer count=recordService.delRecord(id);
//			if(count != 1) {
//				return new Result(false,"删除缺勤记录失败");
//			}
//			return new Result(true,"删除缺勤记录成功");
//		}catch(Exception e) {
//			e.printStackTrace();
//			return new Result(false,"删除缺勤记录失败");
//		}
//	}
	
	@RequestMapping("/delRecord")
	public String delRecord(@Param("id")Integer id) {
		Integer i=recordService.delRecord(id);
		return "redirect:/findRecordAll";
	}
}
