package com.itheima.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.annotation.JsonView;
import com.itheima.domain.Dorm;
import com.itheima.domain.User;
import com.itheima.service.DormService;
import com.itheima.service.StudentService;

import entity.PageResult;
import entity.Result;
import net.sf.jsqlparser.Model;
/*
 * 学生入住管理
 */
@Controller
@RequestMapping("/student")
public class StudentController {
	//注入StudentService对象
	@Autowired
	private StudentService studentService;
	//注入DormService对象
	@Autowired
	private DormService dormService;
	
	@RequestMapping("/findStudentAll")
	public ModelAndView findStudentAll() {
		// 查询全部学生
		List<User> list = studentService.findStudentAll();
		ModelAndView modelAndView = new ModelAndView();
		// 将查询到的数据存放在 ModelAndView的对象中
		modelAndView.addObject("list", list);
		modelAndView.setViewName("liststudent");
		return modelAndView;
	}
	
	//根据ID查询学生，回显到修改页面
	@RequestMapping("/findStudentId")
	public ModelAndView findStudentId(int id,HttpServletRequest request) {
		System.out.println("dhushsjh");
		User stu = studentService.findStudentId(id);
		//request.setAttribute("stu", stu);
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.addObject("stu", stu);
		modelAndView.setViewName("editStu");
		return modelAndView;
	}
		
	// 多条件查询学生
	@RequestMapping("/findStudentMany")
	public ModelAndView findStudentMany(User user) {
		System.out.println("gggggg");
		List<User> list = studentService.findStudentMany(user);
		ModelAndView modelAndView = new ModelAndView();
		// 将查询到的数据存放在 ModelAndView的对象中
		modelAndView.addObject("list", list);
		modelAndView.setViewName("liststudent");
		return modelAndView;
	}
	
	//跳转添加学生页面
//	@RequestMapping("/addstu")
//    public String addstudent() {
//		
//        return "addStu";
//	}
	
	//添加学生
	@ResponseBody
	@RequestMapping("/addStudent")
	public Result addStudent(User user,Dorm dorm) {
		System.out.println(user);
		System.out.println(dorm);
		
		try {
			
			//查询宿舍存不存在 
			Integer num=dormService.fingDormBuild(dorm);
			System.out.println(num);
			if(num <1 ) {
				return new Result(false,"宿舍不存在");
			}
			
			//判断宿舍状态
			Dorm ds=dormService.findDormNum(dorm);
			if(ds.getDorm_status().equals("已满")) {
				return new Result(false,"宿舍已满");
			}
			System.out.println(dorm.getDorm_status());
			
			Integer count=studentService.addStudent(user,dorm);
			System.out.println(count);
			if(count < 1) {
				return new Result(false,"添加学生失败");
			}
			
			//修改宿舍状态
			Dorm cm=dormService.findDormNum(dorm);
			if(cm.getCount()==cm.getMax_num()) {
				Integer sta=dormService.editdormStatus(dorm);
			}
			System.out.println(dorm.getCount());
			System.out.println(dorm.getMax_num());
			return new Result(true,"添加学生成功");
			
			
		}catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "添加学生失败!");
		}
	}
	
	//修改学生信息
	@ResponseBody
	@RequestMapping("/editStudent")
	public Result editStudent(User user,Dorm dorm) {
		
		try {
			System.out.println("进来修改信息了");
			//查询宿舍存不存在 
			Integer num=dormService.fingDormBuild(dorm);
			System.out.println(num);
			if(num <1 ) {
				return new Result(false,"宿舍不存在");
			}
			
			
			
			//判断宿舍状态
			Dorm ds=dormService.findDormNum(dorm);
			if(ds.getDorm_status().equals("已满")) {
				return new Result(false,"宿舍已满");
			}
			System.out.println("地方放电饭锅");
			System.out.println(dorm.getDorm_status());
			
			
			Integer count=studentService.editStudent(user,dorm);
			if(count < 1) {
				return new Result(false,"修改学生信息失败");
			}
			
			
			//修改宿舍状态
			Dorm cm=dormService.findDormNum(dorm);
			if(cm.getCount()==cm.getMax_num()) {
			Integer sta=dormService.editdormStatus(dorm);
			}
			System.out.println(dorm.getCount());
			System.out.println(dorm.getMax_num());
			return new Result(true,"修改学生信息成功");
			
			
		}catch(Exception e){
			e.printStackTrace();
			return new Result(false,"修改宿舍信息失败");
		}
	}
	
	
	//删除学生信息
	@ResponseBody
	@RequestMapping("/delStudent")
	public Result delStudent(Integer id,Dorm dorm) {
		try {
			Integer count = studentService.delStudent(id,dorm);
			if(count < 1) {
				return new Result(false,"删除学生信息失败");
				
			}
			return new Result(true,"删除学生信息成功");
		}catch(Exception e) {
			e.printStackTrace();
			return new Result(false,"删除学生信息失败");
		}
	}
	
	
  }

